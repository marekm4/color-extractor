[![Build status](https://gitlab.com/marekm4/color-extractor/badges/master/build.svg)](https://gitlab.com/marekm4/color-extractor/commits/master)

Simple image color extractor written in Go with no external dependencies.

Demo:

https://color-extractor-demo.herokuapp.com/

Usage:
```go
package main

import (
	...
	"gitlab.com/marekm4/color-extractor"
	...
)

func main() {
	...
	file := "Fotolia_45549559_320_480.jpg"
	imageFile, _ := os.Open(file)
	defer imageFile.Close()

	img, _, _ := image.Decode(imageFile)
	extractedColors := color_extractor.ExtractColors(img)
	...
}
```

Example image:

![Image](https://gitlab.com/marekm4/color-extractor/raw/master/example/Fotolia_45549559_320_480.jpg)

Extracted colors:

![Colors](https://gitlab.com/marekm4/color-extractor/raw/master/example/colors.png)
