package main

import (
	"image"
	_ "image/jpeg"
	"os"

	"image/color"
	"image/png"
	"io"

	"gitlab.com/marekm4/color-extractor"
)

func main() {
	file := "Fotolia_45549559_320_480.jpg"
	imageFile, _ := os.Open(file)
	defer imageFile.Close()

	img, _, _ := image.Decode(imageFile)
	extractedColors := color_extractor.ExtractColors(img)

	paletteFile, _ := os.OpenFile("colors.png", os.O_CREATE|os.O_WRONLY, 0644)
	defer paletteFile.Close()

	createPalette(paletteFile, extractedColors)
}

func createPalette(w io.Writer, colors []color.Color) {
	squareSize := 40
	image := image.NewRGBA(image.Rect(0, 0, len(colors)*squareSize, squareSize))
	for i, color := range colors {
		for j := 0; j < squareSize; j++ {
			for k := 0; k < squareSize; k++ {
				image.Set(i*squareSize+j, k, color)
			}
		}
	}
	png.Encode(w, image)
}
